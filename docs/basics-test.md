In quantum 


## Quantum Information Examples

In classical computing a bit is either `1` or `0`. Typically this represets a voltage applied or not applied on a circuit. 

A _qubit_ is the quantum equivalent of the classical bit. 


### Electron Spin
The spin of an electron is an example of what could constitute a qubit. At the quantum level, particles carry a spin or "angular momentum", as a ball rotating on its axis would. However, on the quantum level, these values are fixed or 'quantised'. When an electrons spin is measured ([e.g. by a magnetic field](https://en.wikipedia.org/wiki/Stern%E2%80%93Gerlach_experiment)), it must be either 'spin up' or 'spin down'. 

This is illustrated in the diagram below showing how (at the quantum level!) the precession direction of a particle's spin is associated with it being either 'up' or 'down':

![image.png](attachment:image.png)


### Polarised Light
dasd



## Qubits and Quantum Logic Gates

Fundamentally, there are two key concepts to grasp with Quantum Computation:
1. Qubits - the quantum equivalent of classical bits (e.g. `0` or `1`) 
2. Quantum Logic Gates  - the quantum equivalent of classical gates (e.g. NOT gate that changes a bit `0` to `1`)

### Qubits
_Dirac notation_ is used to represent the linear algabra of the bits as being either '0' or '1':

$
|0\rangle = \begin{bmatrix}
1 \\
0 
\end{bmatrix} , 
|1\rangle = \begin{bmatrix}
0 \\
1 
\end{bmatrix}
$


### Quantum Logic Gates
Quantum logic gates can be used to alter these states. For example, the $X$ gate (NOT gate) is quantum computationally represented by the following matrix:
<br>
<center>
$
X = \begin{bmatrix}
0 & 1 \\
1 & 0 
\end{bmatrix}
$
</center>

Which you can see, when applied to a qubit, transforms it into it's opposite state:
<br>
<center>
$X|0\rangle  = |1\rangle$  
$X|1\rangle  = |0\rangle$  
</center>

A Hadamard gate is a quantum operation that can put a qubit into a superposition of both |0\rangle and |1\rangle - making use of the nature of qubits over bits. 
<br>
<center>
$
H = \frac{1}{\sqrt{2}}\begin{bmatrix}
1 & 1 \\
1 & -1 
\end{bmatrix}
$
</center>
    
You could think of this in terms of Schrödinger Cat - an alive cat ($|0\rangle$) is put into a state of being both alive and dead ($|1\rangle$) by putting it in a box with a device that has a probabity of 0.5 to kill the cat (represented by a Hadamard gate):

<br>
<center>
$
H|0\rangle  = \frac{1}{\sqrt{2}} (0\rangle +  |1\rangle )
$ 
</center>



```python
import cirq
```


```python
cirq.LineQubit(0)
```




    cirq.LineQubit(0)


