We are already in the age of [Quantum Supremacy](https://www.nature.com/articles/s41586-019-1666-5), the era in which quantum computers are starting to outperform classical ones.

This site will help you, your group or your company become {++Quantum Ready++} by providing you with a tone of free material and workbooks on the basics of Quantum Data Science :atom:

Our goals are to...

- [x]  __Convince you :__
    * [ ] That the quantum era is already here!
    * [ ] That you need to get quantum ready!
- [x]  __Teach you :__   
    * [ ] The basics of quantum information theory
    * [ ] How quantum algorithmns can be written using the latest open-source tools and packages
- [x]  __Show you :__
    * [ ] How data science projects, involving quantum information, may be constructed


![](images/Double_Slit.png)


<center>
[Get in touch with us to learn more!](#){: .md-button .md-button--primary}
</center>

___

## Why should I be bothered about being _quantum_ ready?

![](images/Graphic_1-2.png){: align=left : width=400px }


{==It's the future==}. If that was not convincing, here are what we believe are the most crucial reasons:


### Is your data quantum?
Are you a chemist trying to understand molecular structures, or maybe a particle physicist trying to simulate particle interactions? Then your life is going to get a lot easier if you don't need to _classically_ model data that is intrinsically _quantum_ mechanical.

### How 
The potential for algorithmic speedups when running on a quantum device are enormous.



!!! Question "Will we need to buy a Quantum Computer to be competitive?"
    In most cases - no. 

